function adminWordpress(req, res) {
    /* Redirect */
    if (!/^\/?wp-login\.php/.test(req.url)) {
        res.redirect('/wp-login.php');
        return;
    }
    /* Show Login page */
    const content = this.binding(
        req,
        this.fs.readFileSync(
            this.path.resolve(__dirname, 'ress/pages/wp-login.html'),
            'utf8'
        )
    );
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    res.end(content);
}

module.exports = adminWordpress;
