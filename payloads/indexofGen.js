function indexofGen(req, res) {
    /* Generate */
    const hash = this.sha256(`${this.serial}${req.url}`);
    let links = new Array(2 + (hash.charCodeAt(0) % 10)).fill(0).map((_, i) => {
        return {
            date: new Date(),
            name: this.sha256(`${hash}-${i}`).slice(0, 10),
            size: '-'
        };
    });
    /* Load */
    const content = this.binding(
        req,
        this.fs.readFileSync(
            this.path.resolve(__dirname, 'ress/pages/directory.html'),
            'utf8'
        )
    ).replace(
        /{{LINKS}}/g,
        links
            .map(link => {
                const targetName = link.size === '-' ? `${link.name}/` : '';
                return (
                    '<tr>' +
                    '<td valign="top">&nbsp;</td>' +
                    `<td><a href="${targetName}">${targetName}</a></td>` +
                    `<td align="right">${this.tools.date(
                        'Y-m-d H:i',
                        link.date
                    )}</td>` +
                    `<td align="right">${link.size}</td>` +
                    '<td>&nbsp;</td>' +
                    '</tr>'
                );
            })
            .join('\n')
    );
    /* Render */
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    res.end(content);
}

module.exports = indexofGen;
