module.exports = {
    'admin-custom': require('./adminCustom.js'),
    'admin-wordpress': require('./adminWordpress.js'),
    'indexof-gen': require('./indexofGen.js')
};
