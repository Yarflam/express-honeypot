# Express Honeypot

Captures and directs malicious bots to fake pages. Compatible with `Express`.

**WARN**: The module is not yet finished. It will be published later on NPM.

## Install

Run this command in your `Express` project:

```bash
npm i https://gitlab.com/Yarflam/express-honeypot.git
```

## Usage

You can prepare a configuration JSON file (or JS object) like the one given as an example:

```JSON
[
    {
        "path": "/admin",
        "method": "GET,POST",
        "payload": "admin-custom"
    },
    {
        "path": "/^\\/?(wp-admin\\/|wp-login\\.php)/",
        "method": "GET,POST",
        "payload": "admin-wordpress"
    },
    {
        "path": "/^\\/?(img|static|upload)/",
        "method": "GET",
        "payload": "indexof-gen"
    }
]
```

-   **path**: path of the page to detect (string or regex)
-   **method**: HTTP method types separated by commas
-   **payload**: payload to be used (`admin-custom`, `admin-wordpress`, `indexof-gen`)

Then initialize the module with Express:

```javascript
const Honeypot = require('express-honeypot');

// [...] Prepare the server [...]

/* Before 404: add honeypot */
const myHoneypot = new Honeypot(Honeypot.profile);
myHoneypot.listen(app);
```

Replace `Honeypot.profile` by your configuration.

## Example

Start the example:

```bash
npm run test
```

## Author

Yarflam - _initial worker_
You can thank me if you wish: `0xeF75eEF8AA5b2D4D58C4FF8b0528942a3577456c (ETH)`

## License

Copyright © MIT X11 2022, Yarflam
