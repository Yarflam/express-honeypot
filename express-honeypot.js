const payloads = require('./payloads');
const tools = require('./utils/tools');
const express = require('express');
const sha256 = require('sha256');
const path = require('path');
const fs = require('fs');

class Honeypot {
    constructor(profile) {
        this._profile = profile;
        this._serial = sha256(new Date());
        this._resspath = 'public';
    }

    listen(handle) {
        const utils = {
            path,
            fs,
            sha256,
            tools,
            binding: this.binding.bind(this),
            serial: this._serial
        };
        /* Ressources */
        handle.use(
            `/${this._resspath}/`,
            express.static(path.resolve(__dirname, 'payloads/ress'))
        );
        /* Initialize */
        for (let profile of this._profile) {
            for (let method of profile.method.split(',')) {
                handle[method.toLowerCase()](
                    /^\/.+\/$/.test(profile.path)
                        ? new RegExp(profile.path.slice(1, -1))
                        : profile.path,
                    payloads[profile.payload].bind(utils)
                );
            }
        }
    }

    binding(req, content) {
        return content
            .replace(/{{URL}}/g, req.url)
            .replace(/{{RESS}}/g, `/${this._resspath}`);
    }
}

module.exports = Honeypot;
