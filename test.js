const Honeypot = require('./index');
const express = require('express');
const path = require('path');
const fs = require('fs');

/* Ressources */
const staticPath = path.resolve(__dirname, 'static');
const publicPath = path.resolve(__dirname, 'public');

/* Parameters */
function getTermArgs(key, empty = null) {
    const finder = process.argv.indexOf(key);
    if (finder < 0) return empty || false;
    const out = process.argv.slice(finder + 1, finder + 2)[0];
    return (
        (typeof out === 'string' && !/^--.+$/.test(out) ? out : false) ||
        empty ||
        true
    );
}
const SERVER_HOST = getTermArgs('--host') || '0.0.0.0';
const SERVER_PORT = Number(getTermArgs('--port') || 0) || 3010;

/* Initialize the server */
const app = express();
app.use('/', express.static(publicPath));

/* Before 404: add honeypot */
const myHoneypot = new Honeypot(Honeypot.profile);
myHoneypot.listen(app);

app.get('*', (req, res) => {
    res.statusCode = 404;
    res.setHeader('Content-Type', 'text/html');
    res.end(fs.readFileSync(path.resolve(publicPath, 'error404.html')));
});

app.listen(SERVER_PORT, SERVER_HOST, () => {
    console.log(`Server listening on http://${SERVER_HOST}:${SERVER_PORT}.`);
});
