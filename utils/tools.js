const tools = {
    date: (format = 'Y-m-d H:i', d = 'now') => {
        if (d === 'now') d = new Date();
        if (typeof d === 'string' || typeof d === 'number') d = new Date(d);
        if (typeof format !== 'string') return '';
        /* Format */
        return format
            .replace(/(^|[^\\])Y/g, `$1${d.getFullYear()}`)
            .replace(/(^|[^\\])m/g, `$1${`0${d.getMonth() + 1}`.slice(-2)}`)
            .replace(/(^|[^\\])d/g, `$1${`0${d.getDate()}`.slice(-2)}`)
            .replace(/(^|[^\\])H/g, `$1${`0${d.getHours()}`.slice(-2)}`)
            .replace(/(^|[^\\])i/g, `$1${`0${d.getMinutes()}`.slice(-2)}`)
            .replace(/(^|[^\\])s/g, `$1${`0${d.getSeconds()}`.slice(-2)}`)
            .replace(/\\([^\\])/g, '$1');
    }
};

module.exports = tools;
